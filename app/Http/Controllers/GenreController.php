<?php

namespace App\Http\Controllers;

use App\Http\Requests\Genre\GenreStoreRequest;
use App\Http\Requests\Genre\GenreUpdateRequest;
use App\Models\Genre;

class GenreController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $genres = Genre::get();
        return view('pages.genres.index',
            [
                'headers' => [
                    [
                        'name' => __('genre.table.id'),
                        'value' => 'id',
                    ],
                    [
                        'name' => __('genre.table.name'),
                        'value' => 'name'
                    ],
                ],
                'data' => $genres,
                'nameUrl' => 'genre.destroy'
            ]);
    }

    public function create(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('pages.genres.create', [
            'url' => route('genre.store')
        ]);
    }

    public function store(GenreStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        Genre::create($request->validated());
        return redirect()->route('genre.index');
    }

    public function edit(Genre $genre): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('pages.genres.edit', [
            'url' => route('genre.update', $genre->id)
        ]);
    }

    public function update(GenreUpdateRequest $request, Genre $genre): \Illuminate\Http\RedirectResponse
    {
        $genre->update($request->validated());
        return redirect()->route('genres.index');
    }

    public function destroy(Genre $genre): \Illuminate\Http\RedirectResponse
    {
        $movies = $genre->movies;
        $movies->each(function ($movie) use ($genre) {
            $movie->genres()->detach([$genre->id]);
        });
        $genre->delete();
        return redirect()->route('genre.index');
    }
}
