<?php

namespace App\Http\Controllers;

use App\Http\Requests\Movie\MovieStoreRequest;
use App\Http\Requests\Movie\MovieUpdateRequest;
use App\Models\Genre;
use App\Models\Movie;

class MovieController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $movies = Movie::get();
        return view('pages.movies.index', [
            'headers' => [
                [
                    'name' => __('movie.table.id'),
                    'value' => 'id',
                ],
                [
                    'name' => __('movie.table.title'),
                    'value' => 'title'
                ],
                [
                    'name' => __('movie.table.description'),
                    'value' => 'description',
                ],
                [
                    'name' => __('movie.table.release_date'),
                    'value' => 'release_date',
                ],
            ],
            'data' => $movies,
            'nameUrl' => 'movie.destroy'
        ]);
    }

    public function create(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('pages.movies.create', [
            'genres' => Genre::get(),
            'url' => route('movie.store')
        ]);
    }

    public function store(MovieStoreRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $movie = Movie::create($data);
        $movie->genres()->sync($data['genres']);
        return redirect()->route('movie.index');
    }

    public function edit(Movie $movie): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('pages.movies.edit', [
            'movie' => $movie,
            'genres' => Genre::get(),
            'genreToMovie' => $movie->genres->pluck('id'),
            'url' => route('movie.update', $movie->id)
        ]);
    }

    public function update(MovieUpdateRequest $request, Movie $movie): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $movie->update($data);
        $movie->genres()->sync($data['genres']);
        return redirect()->route('movie.index');
    }

    public function destroy(Movie $movie): \Illuminate\Http\RedirectResponse
    {
        $movie->delete();
        return redirect('/movie');
    }
}
