<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use Illuminate\Http\Request;

class MovieApiController extends Controller
{
    public function getAllMovies(): \Illuminate\Http\JsonResponse
    {
        $movies = Movie::get();
        return response()->json($movies);
    }

    public function getMovie(Movie $movie): \Illuminate\Http\JsonResponse
    {
        return response()->json($movie);
    }
}
