@extends('pages.index')
@section('content')
    <div class="row">
        <div class="col">
            <a type="button" class="btn btn-primary" href="{{ url('genre/create') }}">{{ __('table.action.add') }}</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @include('components.table', [
                'headers' => $headers,
                'data' => $data,
                'url' => $nameUrl
                ])
        </div>
    </div>
@endsection
