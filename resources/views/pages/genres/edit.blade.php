@extends('components.form')
@section('field')
@method('PATCH')
<div class="mb-3">
    <label for="title" class="form-label">Name</label>
    <input class="form-control" name="name" placeholder="Name" value="{{ $movie->title }}">
</div>
@endsection
