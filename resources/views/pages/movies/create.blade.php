@extends('components.form')
@section('field')
<div class="mb-3">
    <label for="title" class="form-label">Title</label>
    <input class="form-control" name="title" placeholder="Title">
</div>
<div class="mb-3">
    <label for="description" class="form-label">Description</label>
    <textarea class="form-control" name="description" rows="3"></textarea>
</div>
<div class="mb-3">
    <div class="form-group">
        <label for="inputDate">Введите дату:</label>
        <input type="date" name="release_date" class="form-control">
    </div>
</div>
<div class="mb-3">
    <div class="form-group">
        <label for="inputDate">Выберите жанр:</label>
        <select class="selectpicker" multiple name="genres[]" data-live-search="true">
            @foreach($genres as $genre)
                <option value="{{ $genre->id }}">{{ $genre->name }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection
