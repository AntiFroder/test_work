@extends('app')
@section('nav_bar')
    @include('components.nav-bar')
@endsection
@section('main')
    <div class="container">
        @yield('content')
    </div>
@endsection
@yield('script')
