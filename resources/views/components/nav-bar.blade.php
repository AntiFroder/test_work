<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link" id="movie" href="{{ url('/movie') }}">{{ __('nav-bar.movie') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="genre" href="{{ url('/genre') }}">{{ __('nav-bar.genre') }}</a>
    </li>
</ul>
@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const url = window.location.href
            if (url.includes('movie')) {
                document.getElementById('movie').classList.add('active')
                document.getElementById('genre').classList.remove('active')
            } else {
                document.getElementById('genre').classList.add('active')
                document.getElementById('movie').classList.remove('active')
            }
        })
    </script>
@endsection
