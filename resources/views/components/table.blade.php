<table class="table table-hover align-middle">
    <thead>
    <tr>
        @foreach($headers as $header)
            <th style="width: 20%">{{ $header['name'] }}</th>
        @endforeach
        <th>{{ __('table.action.column_name') }}</th>
    </tr>
    </thead>
    <tbody class="table-group-divider">
    @foreach($data as $entity)
        <tr>
            @for($i = 0; $i < count($headers); $i++)
                <td>{{ $entity->{$headers[$i]['value']} }}</td>
            @endfor
            <td id="actionBtn">
                <a class="btn btn-info" id="edit_{{$entity->id}}" href="">{{ __('table.action.edit') }}</a> |
                <form method="POST" action="{{ route($url, $entity->id) }}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">{{ __('table.action.delete') }}</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const url = window.location.href + '/'
        const table = document.querySelector('table')
        const actionBtn = table.querySelectorAll('td#actionBtn > a')
        actionBtn.forEach(function (item, i) {
            let partUrl = item.id.split('_')
            item.setAttribute('href', url + partUrl[1] + '/' + partUrl[0])
        })
    })
</script>
