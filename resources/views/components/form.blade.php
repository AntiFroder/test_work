@extends('pages.index')
@section('content')
<form action="{{ $url }}" method="post">
    @csrf
    @yield('field')
    <button type="submit" class="btn btn-success">Save</button>
    <a type="button" class="btn btn-secondary" href="{{ url('/movie') }}">Back</a>
</form>
    @yield('script')
@endsection
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
