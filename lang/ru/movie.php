<?php declare(strict_types=1);

return [
    'table' => [
        'id' => 'ID',
        'title' => 'Наименование',
        'description' => 'Описание',
        'release_date' => 'Дата выхода',
    ],
];
