<?php declare(strict_types=1);

return [
    'action' => [
        'column_name' => 'Действие',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
        'add' => 'Добавить'
    ]
];
