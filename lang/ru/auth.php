<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы аутентификации
    |--------------------------------------------------------------------------
    |
    | Следующие языковые ресурсы используются во время аутентификации для
    | различных сообщений которые мы должны вывести пользователю на экран.
    | Вы можете свободно изменять эти языковые ресурсы в соответствии
    | с требованиями вашего приложения.
    |
    */

    'failed' => 'Неверный Логин или Пароль.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',

    'not_active' => 'Ваш аккаунт заблокирован',

    'two_factor_code' => 'Неверный двухфакторый код.',

    'sending_two_factor_code_failed' => 'Не удалось направить код двухфакторной аутентификации. Обратитесь к администратору',
];
