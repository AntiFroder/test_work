<?php

namespace Database\Seeders;

use App\Models\Genre;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $genreType = [
            'Экшн',
            'Блокбастер',
            'Мультфильм',
            'Комедия',
            'Ужасы',
            'Мюзикл',
        ];
        DB::table('genres')->truncate();
        foreach ($genreType as $item) {
            DB::table('genres')->insert([
                'name' => $item,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
