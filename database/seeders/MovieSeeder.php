<?php

namespace Database\Seeders;

use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('movies')->truncate();
        Movie::factory()
            ->count(20)
            ->create()->each(function ($movie) {
                $movie->genres()->attach(
                    Genre::pluck('id')->random(2)->all()
                );
            });
    }
}
